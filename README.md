# Bitpanda QA 2

Postman API testing

## Case 

Write automated tests for a REST API service:

● Implement REST API automated tests for a “location” (navigation) service of
your choice (e.g. https://developer.here.com/). The idea of this exercise is to
implement automated tests for creating data, modifying existing data or
deleting data (for example, you can create your own map with pins on it);

○ You may pick any REST API provider that offers such a service (as
long as it's free to use);

○ Implement test cases of sending location information to the map. Test
sending proper information, invalid information, incorrect format and
any other possible edge cases;

○ Use at least three different HTTP response codes in your tests (for
instance 2xx, 3xx, 4xx);

○ Report any bugs you find by writing a bug ticket;

● Please share with us (additionally) created API definitions in a postman
collection form.

## Solution

As long as postman collections required - I have tried to make API testing in the same tool.

- Tests are created via Postman GUI and executed with Newman CLI in CI to this project. You can check previous run logs or start a new 
pipeline via `CI/CD` on the left.
- Tests use single API endpoint from here.com DataHub API - which adds 'Feature' geo point to some precreated collection.
- Token for API access is stored openly in CI, though irl that would be in a secret storage ofc, or at least under protected Gitlab variables
- There is an API limits for free use - but iirc there is more than enough for you to play around with CI and tests if you wish so
- Test themselves are simple checks via Postman scripting sandbox
